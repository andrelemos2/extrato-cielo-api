# extrato-cielo-api

Simples API para listagem do extrato.

## Pré-requisitos:
- Porta 8080 backend

## Construindo aplicação

```bash
  mvn spring-boot:run
```

## Backlog:
https://trello.com/b/L2J2RgF0/extrato-cielo-api

## API Docs:
http://localhost:8080/cielo/swagger-ui.html

## Técnologias utilizadas:
- Java 8
- Spring Boot
- Jackson 2.0
- Swagger 2
- PojoToJson http://www.jsonschema2pojo.org/

