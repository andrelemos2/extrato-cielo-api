package com.adquirente.cielo.extrato.api.util;

public interface Constantes {
    public interface SWAGGER {
        public static final String BASE_PACKAGE = "com.adquirente.cielo.extrato.api";
        public static final String TITULO = "Extrato API";
        public static final String DESCRICAO = "Gerador de extrato";
        public static final String RESOURCE_HANDLER = "swagger-ui.html";
        public static final String RESOURCE_LOCATIONS = "classpath:/META-INF/resources/";
        public static final String VERSION_PROJECT = "1.2.0";
    }
}
