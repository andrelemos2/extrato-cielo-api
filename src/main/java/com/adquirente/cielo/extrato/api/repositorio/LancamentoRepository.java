package com.adquirente.cielo.extrato.api.repositorio;

import com.adquirente.cielo.extrato.api.configuracao.ConfiguracaoExtrato;
import com.adquirente.cielo.extrato.api.modelo.Lancamento;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
public class LancamentoRepository {

    @Autowired
    private ConfiguracaoExtrato configuracaoExtrato;

    public Lancamento buscaLancamentos() throws IOException {
        Resource resource = configuracaoExtrato.getResource();
        Lancamento lancamentos = new ObjectMapper().readValue(resource.getFile(), Lancamento.class);
        return lancamentos;
    }
}
