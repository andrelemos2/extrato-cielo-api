package com.adquirente.cielo.extrato.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExtratoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExtratoApiApplication.class, args);
    }

}

