
package com.adquirente.cielo.extrato.api.modelo;

import java.util.List;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "totalControleLancamento",
    "listaControleLancamento",
    "indice",
    "tamanhoPagina",
    "totalElements"
})
public class Lancamento {

    @JsonProperty("totalControleLancamento")
    @Valid
    private TotalControleLancamento totalControleLancamento;
    @JsonProperty("listaControleLancamento")
    @Valid
    private List<ListaControleLancamento> listaControleLancamento = null;
    @JsonProperty("indice")
    private Long indice;
    @JsonProperty("tamanhoPagina")
    private Long tamanhoPagina;
    @JsonProperty("totalElements")
    private Long totalElements;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Lancamento() {
    }

    /**
     * 
     * @param listaControleLancamento
     * @param totalControleLancamento
     * @param totalElements
     * @param indice
     * @param tamanhoPagina
     */
    public Lancamento(TotalControleLancamento totalControleLancamento, List<ListaControleLancamento> listaControleLancamento, Long indice, Long tamanhoPagina, Long totalElements) {
        super();
        this.totalControleLancamento = totalControleLancamento;
        this.listaControleLancamento = listaControleLancamento;
        this.indice = indice;
        this.tamanhoPagina = tamanhoPagina;
        this.totalElements = totalElements;
    }

    @JsonProperty("totalControleLancamento")
    public TotalControleLancamento getTotalControleLancamento() {
        return totalControleLancamento;
    }

    @JsonProperty("totalControleLancamento")
    public void setTotalControleLancamento(TotalControleLancamento totalControleLancamento) {
        this.totalControleLancamento = totalControleLancamento;
    }

    @JsonProperty("listaControleLancamento")
    public List<ListaControleLancamento> getListaControleLancamento() {
        return listaControleLancamento;
    }

    @JsonProperty("listaControleLancamento")
    public void setListaControleLancamento(List<ListaControleLancamento> listaControleLancamento) {
        this.listaControleLancamento = listaControleLancamento;
    }

    @JsonProperty("indice")
    public Long getIndice() {
        return indice;
    }

    @JsonProperty("indice")
    public void setIndice(Long indice) {
        this.indice = indice;
    }

    @JsonProperty("tamanhoPagina")
    public Long getTamanhoPagina() {
        return tamanhoPagina;
    }

    @JsonProperty("tamanhoPagina")
    public void setTamanhoPagina(Long tamanhoPagina) {
        this.tamanhoPagina = tamanhoPagina;
    }

    @JsonProperty("totalElements")
    public Long getTotalElements() {
        return totalElements;
    }

    @JsonProperty("totalElements")
    public void setTotalElements(Long totalElements) {
        this.totalElements = totalElements;
    }

}
