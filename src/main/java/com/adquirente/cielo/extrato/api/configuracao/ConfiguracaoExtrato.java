package com.adquirente.cielo.extrato.api.configuracao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class ConfiguracaoExtrato {

    @Value("classpath:lancamento-conta-legado.json")
    private Resource resource;

    public Resource getResource() {
        return resource;
    }
}
