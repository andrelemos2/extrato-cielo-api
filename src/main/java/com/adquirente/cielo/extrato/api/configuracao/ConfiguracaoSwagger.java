package com.adquirente.cielo.extrato.api.configuracao;

import com.adquirente.cielo.extrato.api.util.Constantes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ConfiguracaoSwagger {
	
	@Bean
	public Docket apiConfigDocs() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage(Constantes.SWAGGER.BASE_PACKAGE))
				.paths(PathSelectors.any())
				.build()
                .apiInfo(informacoesAPI());
	}
	
	private ApiInfo informacoesAPI() {
        return new ApiInfoBuilder()
                .title(Constantes.SWAGGER.TITULO)
                .description(Constantes.SWAGGER.DESCRICAO)
                .version(Constantes.SWAGGER.VERSION_PROJECT)
                .build();
    }
}
