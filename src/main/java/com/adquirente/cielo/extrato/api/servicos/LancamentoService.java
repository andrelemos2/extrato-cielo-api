package com.adquirente.cielo.extrato.api.servicos;

import com.adquirente.cielo.extrato.api.modelo.Lancamento;
import com.adquirente.cielo.extrato.api.repositorio.LancamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class LancamentoService {

    @Autowired
    private LancamentoRepository lancamentoRepository;

    public Lancamento buscaLancamentos() throws IOException {
        return lancamentoRepository.buscaLancamentos();
    }
}
