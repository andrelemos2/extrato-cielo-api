package com.adquirente.cielo.extrato.api.recursos;

import com.adquirente.cielo.extrato.api.modelo.Lancamento;
import com.adquirente.cielo.extrato.api.servicos.LancamentoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@CrossOrigin
@RestController
@RequestMapping(value = "/extrato")
public class LancamentoResource {

    private static final Logger log = LoggerFactory.getLogger(LancamentoResource.class);

    @Autowired
    private LancamentoService lancamentoService;

    @GetMapping
    public ResponseEntity<Lancamento> buscarLancamentos() {
        try {
            return ResponseEntity.ok(lancamentoService.buscaLancamentos());
        } catch (IOException e) {
            log.error("Erro ao gerar extrato:", e);
            return ResponseEntity.notFound().build();
        }
    }
}
